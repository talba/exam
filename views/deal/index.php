<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DealSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Deals';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="deal-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
		 <?php if (\Yii::$app->user->can('createDeal')) { ?>
        <?= Html::a('Create Deal', ['create'], ['class' => 'btn btn-success']) ?>
		<?php } ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name', 
			[// the name of the lead
				'attribute' => 'leadId',
				'label' => 'Lead',
				'format' => 'raw',
				'value' => function($model){
					return Html::a($model->dealLead->name, 
					['lead/view', 'id' => $model->dealLead->id]);
				},
				'filter'=>Html::dropDownList('DealSearch[leadId]', $lead, $leads, ['class'=>'form-control']),
			],
			[
				'attribute' => 'amount',
				'label' => 'Amount',
				'format' => 'raw',
				'value' => function($model){
					return $model->amount;
				},
				'filter'=>Html::dropDownList('DealSearch[amount]', $amount, $amounts, ['class'=>'form-control']),
			],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
