<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\models\Lead;

/**
 * This is the model class for table "deal".
 *
 * @property integer $id
 * @property string $name
 * @property integer $leadId
 * @property integer $amount
 */
class Deal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'deal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'leadId', 'amount'], 'required'],
            [['leadId', 'amount'], 'integer'],
            [['name'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'leadId' => 'Lead',
            'amount' => 'Amount',
        ];
    }
	
	public static function getDeals()
	{
		$deals = ArrayHelper::
					map(self::find()->all(), 'id', 'amount');
		return $deals;						
	}
	
	public static function getAmounts()
	{
		$deals = ArrayHelper::
					map(self::find()->all(), 'id', 'amount');
		return $deals;						
	}
	
	public static function getAmuntsWithAllAmunts()
	{
		$amounts = self::getAmounts();
		$amounts[-1] = 'All Deals';
		$amounts = array_reverse ( $amounts, true );
		return $amounts;	
	
	}
	
	public static function getLeadsWithAllLeads()
	{
		$leads = self::getLeads();
		$leads[-1] = 'All Deals';
		$leads = array_reverse ( $leads, true );
		return $leads;	
	
	}
	
	public function getDealLead()
	{
		return $this->hasOne(Lead::className(), ['id'=>'leadId']);
	}
	
	public static function getLeads()
	{
		$allLeads = Lead::find()->all();
		$allLeadsArray = ArrayHelper::
					map($allLeads, 'id', 'name');
		return $allLeadsArray;						
	}
	

}
